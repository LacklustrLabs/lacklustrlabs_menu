#include "Arduino.h"
#include "lacklustrlabs_menu_ssd1306.h"

using namespace lacklustrlabs;

constexpr int8_t OLED_RESET=4;
Adafruit_SSD1306 display(OLED_RESET);

RenderContext context(display);

void callback(NumericMenuItem<RenderContext>* menuitem) {
  Serial.print("bool changed ");
  Serial.println(reinterpret_cast<uintptr_t>(menuitem));
}

MenuItem<RenderContext>* subMenuItems2[] = {
    new BackMenuItem<RenderContext>("<-- back"),
    new MenuItem<RenderContext>("sub2 first"),
    (new NumericMenuItem<RenderContext>("sub2 bool", 0, 0, 1, 1, boolFormat, callback)),
    new NumericMenuItem<RenderContext>("sub2 float", 5, 0, 10,-.5, floatFormat), // will decrement on 'up event'
    new NumericMenuItem<RenderContext>("sub2 int", 5, 0, 10, 1, intFormat),      // will increment on 'up event'
    new MenuItem<RenderContext>("sub2 last"), nullptr };

SubMenu<RenderContext> subMenu2("2 Sub menu 2", subMenuItems2);

MenuItem<RenderContext>* subMenuItems1[] = {
    new BackMenuItem<RenderContext>("<-- back"),
    &subMenu2,
    new MenuItem<RenderContext>("sub1 first"),
    new NumericMenuItem<RenderContext>("sub1 bool", 0, 0, 1, 1, boolFormat),
    new NumericMenuItem<RenderContext>("sub1 float", 5, 0, 10, -.5, floatFormat), // will decrement on 'up event'
    new NumericMenuItem<RenderContext>("sub1 int", 5, 0, 10, 1, intFormat),       // will increment on 'up event'
    new MenuItem<RenderContext>("sub1 last"), nullptr };

SubMenu<RenderContext> subMenu1("1 Sub menu 1", subMenuItems1);

MenuItem<RenderContext>* menuItems[] = {
    &subMenu1,
    new MenuItem<RenderContext>("0 first"),
    (new NumericMenuItem<RenderContext>("0 bool", 0, 0, 1, 1, boolFormat, callback)),
    new NumericMenuItem<RenderContext>("0 float", 5, 0, 10,-.5, floatFormat), // will decrement on 'up event'
    new NumericMenuItem<RenderContext>("0 int", 5, 0, 10, 1, intFormat),      // will increment on 'up event'
    new MenuItem<RenderContext>("0 last"), nullptr };

Menu<RenderContext> aMenu(context, "0 Main Menu", menuItems);


void setup() {
  Serial.begin(19200);
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, SSD1306_I2C_ADDRESS);  // initialize with the I2C addr 0x3D (for the 128x64)
  // init done
  
  // Show image buffer on the display hardware.
  // Since the buffer is intialized with an Adafruit splashscreen
  // internally, this will display the splashscreen.
  display.display();
  delay(2000);

  // Clear the buffer.
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  
  Serial.println("Display is initiated");
  aMenu.render();
}

void loop() {
  char c = Serial.read();
  bool updated = false;
  switch (c) {
  case 'w':
    aMenu.upEvent();
    Serial.println("upEvent()");
    updated = true;
    break;
  case 's':
    aMenu.downEvent();
    Serial.println("downEvent()");
    updated = true;
    break;
  case 'd':
    aMenu.selectEvent();
    Serial.println("selectEvent()");
    updated = true;
    break;
  case 'a':
    aMenu.backEvent();
    Serial.println("backEvent()");
    updated = true;
    break;
  case 'f':
    // will set NumericMenuItems to default value
    aMenu.longPressEvent();
    Serial.println("longPressEvent()");
    updated = true;
    break;
#ifndef ARDUINO:
  case 'q':
    Serial.println(F("bye"));
    exit(1);
#endif
  }
  if (updated) {
    aMenu.render();
  }

}