/*
 * lacklustrlabs_menu_serial.h
 *
 *  Created on: Apr 5, 2017
 *      Author: lacklustrlabs
 */
#pragma once

#include "Arduino.h"
#include "lacklustrlabs_menu.h"


struct RenderContext {
  RenderContext() = delete;
  RenderContext( const RenderContext& other ) = delete; // non construction-copyable
  RenderContext& operator=( const RenderContext& ) = delete; // non copyable
  RenderContext(Stream& stream):_stream(stream) {}

  Stream& _stream;
};

namespace lacklustrlabs {

template<>
void MenuItem<RenderContext>::renderV(RenderContext& context) const {
  context._stream.println(getNameV());
}

template<>
void NumericMenuItem<RenderContext>::renderV(RenderContext& context) const {
  context._stream.println(getNameV());
}

template<>
void Menu<RenderContext>::setupContext() {
}

template<>
void Menu<RenderContext>::render() {
  SubMenu<RenderContext> *parent = _current->getParent();
  //_context._stream.print("Parent: ");
  //_context._stream.print((void*)parent);
  //_context._stream.print(": ");
  if (parent) {
    parent->renderV(_context);
  } else {
    _context._stream.println("error, no parent");
  }
  //_context._stream.print("Current: ");
  //_context._stream.print((void*)_current);
  //_context._stream.print(": ");
  _current->renderV(_context);
}

} // namespace lacklustrlabs

String progressbar(const lacklustrlabs::NumericMenuItem<RenderContext>* item, uint8_t width = 12){
  // make room for a ' ' at the end and the terminating 0
  char graphics[width+2];
  // fill the string with '-'
  for (int i=0; i<width; i++)
    graphics[i] = '-';
  // insert a '|' at the relative _value position
  graphics[ int( (width-1)*(item->_value-item->_minValue)/(item->_maxValue-item->_minValue))] = '|';
  graphics[width] = ' ';
  graphics[width+1] = 0;
  return graphics;
}

String boolFormat(const lacklustrlabs::NumericMenuItem<RenderContext>* item) {
  if (item->isActiveV())
    return progressbar(item) + String((bool)item->getValue());
  else
    return item->getName() + " " + String((bool)item->getValue());
}

String floatFormat(const lacklustrlabs::NumericMenuItem<RenderContext>* item) {
  if (item->isActiveV())
    return progressbar(item) + String(item->getValue());
  else
    return item->getName() + " " + String(item->getValue());
}

String intFormat(const lacklustrlabs::NumericMenuItem<RenderContext>* item) {
  if (item->isActiveV())
    return progressbar(item) + String((int)item->getValue());
  else
    return item->getName() + " " + String((int)item->getValue());
}
