/*
 * lacklustrlabs_menu.h
 *
 *  Created on: Mar 27, 2017
 *      Author: lacklustrlabs
 */

#pragma once

#include <Arduino.h>

#ifndef LACKLUSTRLABS_MENU_DEBUG
#define LACKLUSTRLABS_MENU_DEBUG(x)
#endif

namespace lacklustrlabs {

template <class RenderContext>
class SubMenu;

template <class RenderContext>

class MenuItem {
protected:
  String _name;
  SubMenu<RenderContext> *_parent = nullptr;

public:
  MenuItem(const String name) :
      _name(name) {
  }

  MenuItem() = delete;
  MenuItem(const MenuItem& other) = delete; // non construction-copyable
  MenuItem& operator=(const MenuItem&) = delete; // non copyable

  virtual ~MenuItem() {
  }

  virtual MenuItem<RenderContext>* upEventV(uint16_t n __attribute__((unused))) {
    return this;
  }

  virtual MenuItem<RenderContext>* downEventV(uint16_t n __attribute__((unused))) {
    return this;
  }

  virtual MenuItem<RenderContext>* selectEventV() {
    return this;
  }

  virtual MenuItem<RenderContext>* longPressEventV() {
    return this;
  }

  virtual bool isActiveV() const {
    return false;
  }

  virtual void deactivateV() {
  }

  virtual bool isMenuV() const {
    return false;
  }

  String getName() const {
    return _name;
  }

  virtual String getNameV() const {
    return _name;
  }

  virtual void renderV(RenderContext& r) const {
  }

  virtual void setNameV(const String & newName) {
    _name = newName;
  }

  SubMenu<RenderContext>* getParent() {
    return _parent;
  }

  /**
   * TODO: This should be protected
   */
  void setParent(SubMenu<RenderContext> *aMenu) {
    _parent = aMenu;
  }
};

template<typename RenderContext>
class BackMenuItem: public MenuItem<RenderContext> {

public:
  BackMenuItem(const String name) : MenuItem<RenderContext>(name) {
  }

  virtual MenuItem<RenderContext>* selectEventV() {
    SubMenu<RenderContext> *parent = this->getParent();
    if(parent) {
      return parent;
    } else {
      LACKLUSTRLABS_MENU_DEBUG(Serial.println("This should never happen, set debug breakpoint here"));
      return this;
    }
  }

};

template<typename RenderContext>
class ToggleMenuItem: public MenuItem<RenderContext> {

public:
  ToggleMenuItem(const String name1, const String name2,
      void (*callback)(ToggleMenuItem<RenderContext>* item)=nullptr) :
      MenuItem<RenderContext>(name1), _name2(name2), _toggle(false),
      _callback(callback) {
  }

  void setCallback(void (*callback)(ToggleMenuItem<RenderContext>* item)) {
    _callback = callback;
  }

  virtual MenuItem<RenderContext>* selectEventV() {
    _toggle = !_toggle;
    if(_callback) _callback(this);
    return this;
  }

  virtual String getNameV() const {
    return _toggle?this->_name: _name2;
  }

  bool getToggle() const {
      return _toggle;
  }

protected:
  String _name2;
  bool _toggle;
  void (*_callback)(ToggleMenuItem<RenderContext>* item);
};

template<typename RenderContext>
class NumericMenuItem: public MenuItem<RenderContext> {

  NumericMenuItem(const NumericMenuItem& other) = delete; // non construction-copyable
  NumericMenuItem& operator=(const NumericMenuItem&) = delete; // non copyable
  NumericMenuItem() = delete;

public:
  NumericMenuItem(const String name, float defaultValue, float minValue, float maxValue, float increment,
                  String (*nameFormat)(const NumericMenuItem<RenderContext>* item)=nullptr,
                  void (*callback)(NumericMenuItem<RenderContext>* item)=nullptr ) :
      MenuItem<RenderContext>(name), _value(defaultValue), _defaultValue(defaultValue),
      _minValue(minValue), _maxValue(maxValue), _increment(increment), _isActive(false),
      _callback(callback), _nameFormat(nameFormat) {
  }

  virtual ~NumericMenuItem() {
  }

  virtual String getNameV() const {
    if (_nameFormat)
      return _nameFormat(this);

    if (_isActive)
      return String("(") + MenuItem<RenderContext>::_name + " " + String(_value) + ")";
    else
      return MenuItem<RenderContext>::_name + " " + String(_value);
  }

  virtual MenuItem<RenderContext>* selectEventV() {
    _isActive = !_isActive;
    return this;
  }

  virtual MenuItem<RenderContext>* longPressEventV() {
    float oldValue = _value;
    _value = _defaultValue;
    _isActive = false;
    if (_callback && oldValue!=_value) {
      _callback(this);
    }
    return this;
  }

  virtual MenuItem<RenderContext>* upEventV(uint16_t n=1) {
    float oldValue = _value;
    _value += _increment*n;
    if (_increment > 0){
      _value = min(_value, _maxValue);
    } else {
      _value = max(_value, _minValue);
    }
    if (_callback && oldValue!=_value) {
      _callback(this);
    }
    return this;
  }

  virtual MenuItem<RenderContext>* downEventV(uint16_t n=1) {
    float oldValue = _value;
    _value -= _increment*n;
    if (_increment > 0){
      _value = max(_value, _minValue);
    } else {
      _value = min(_value, _maxValue);
    }
    if (_callback && oldValue!=_value) {
      _callback(this);
    }
    return this;
  }

  virtual bool isActiveV() const {
    return _isActive;
  }

  virtual void deactivateV() {
    _isActive = false;
  }
  
  float getValue() const {
    return _value;
  }
  
  virtual void renderV(RenderContext& r) const {
    LACKLUSTRLABS_MENU_DEBUG(Serial.println("you forgot to implement a NumericMenuItem::render method"));
  }

  float _value;
  float _defaultValue;
  float _minValue;
  float _maxValue;
  float _increment;
  bool _isActive;
  void (*_callback)(NumericMenuItem<RenderContext>* item);
  String (*_nameFormat)(const NumericMenuItem<RenderContext>* item);
};

template <class RenderContext>
class SubMenu : public MenuItem<RenderContext> {
public:

  SubMenu(const String name, MenuItem<RenderContext>** items, int numitems=-1):
    MenuItem<RenderContext>(name), _items(items), _numitems(numitems), _currentI(-1) {
    if (_numitems < 0 ) {
      int i = 0;
      while(true) {
        if (_items[i]==nullptr ) {
          _numitems = i;
          break;
        }
        i++;
      }
    }
    for(int i=0; i<_numitems; i++){
      _items[i]->setParent(this);
    }
  }

  virtual MenuItem<RenderContext>* selectEventV() {
    if (_currentI < 0) {
      _currentI = 0;
    }
    return _items[_currentI];
  }

  virtual bool isMenuV() const {
    return true;
  }

  virtual bool isActive() {
    return _currentI > -1;
  }

  virtual void deactivateV() {
    _currentI = -1;
  }

  MenuItem<RenderContext>* upEventV(uint16_t n=1) {
    MenuItem<RenderContext>*current = _items[SubMenu<RenderContext>::_currentI];
    if (current->isActiveV()) {
      current->upEventV(n);
    } else if (SubMenu<RenderContext>::_currentI > 0) {
      SubMenu<RenderContext>::_currentI = max(SubMenu<RenderContext>::_currentI-n, 0);
    }
    return _items[SubMenu<RenderContext>::_currentI];
  }

  MenuItem<RenderContext>* downEventV(uint16_t n=1) {
    MenuItem<RenderContext>*current = _items[SubMenu<RenderContext>::_currentI];
    if (current->isActiveV()) {
      current->downEventV(n);
    } else if (SubMenu<RenderContext>::_currentI < SubMenu<RenderContext>::_numitems - 1) {
      SubMenu<RenderContext>::_currentI = min(SubMenu<RenderContext>::_currentI+n, (uint16_t)SubMenu<RenderContext>::_numitems - 1);
    }
    return _items[SubMenu<RenderContext>::_currentI];
  }

protected:
  MenuItem<RenderContext>** _items = nullptr;
  int _numitems = 0;
  int _currentI = -1; // if -1 this menu is not selected
};

template <class RenderContext>
class Menu : protected SubMenu<RenderContext> {
protected:
  RenderContext& _context;
  MenuItem<RenderContext>* _current = nullptr;

  Menu(const Menu& other) = delete; // non construction-copyable
  Menu& operator=(const Menu&) = delete; // non copyable
  Menu() = delete;

public:
  Menu(RenderContext& context, const String name, MenuItem<RenderContext>** items, int numitems=-1) :
    SubMenu<RenderContext>(name, items, numitems), _context(context), _current(items[0]) {
    this->_currentI = 0;
    setupContext();
  }

  void render() {  // This implementation is later replaced with a static polymorphic method
    LACKLUSTRLABS_MENU_DEBUG(Serial.println("Menu::render stub implementation - this method should never be called"));
    //this->assert(false, F("Menu::render stub implementation - this method should never be called"));
  }

  void setupContext() {  // This implementation is later replaced with a static polymorphic method
    LACKLUSTRLABS_MENU_DEBUG(Serial.println("Menu::setupContext() stub implementation - this method should never be called"));
    //this->assert(false, F("Menu::render stub implementation - this method should never be called"));
  }

  MenuItem<RenderContext>* upEvent(uint16_t n=1) {
    if (_current->isActiveV()) {
      _current = _current->upEventV(n);
    } else {
      SubMenu<RenderContext>* parent = _current->getParent();
      if (parent){
        // delegate to the virtual MenuItem::upEvent() method
        _current = parent->upEventV(n);
      } else {
        _current = SubMenu<RenderContext>::upEventV(n);
      }
    }
    return _current;
  }

  MenuItem<RenderContext>* downEvent(uint16_t n=1) {
    if (_current->isActiveV()) {
      _current = _current->downEventV(n);
    } else {
      SubMenu<RenderContext>* parent = _current->getParent();
      if (parent){
        // delegate to the virtual MenuItem::downEvent() method
        _current = parent->downEventV(n);
      } else {
        _current = SubMenu<RenderContext>::downEventV(n);
      }
    }
    return _current;
  }

  MenuItem<RenderContext>* selectEvent() {
    // delegate to the virtual MenuItem::selectEvent() method
    _current = _current->selectEventV();
    return _current;
  }

  MenuItem<RenderContext>* backEvent() {
    _current->deactivateV();
    SubMenu<RenderContext> *parent = _current->getParent();
    if(parent && parent->getParent()) {
      parent->deactivateV();
      _current = parent;
    }
    return _current;
  }

  MenuItem<RenderContext>* longPressEvent() {
    _current = _current->longPressEventV();
    return _current;
  }

};
}
