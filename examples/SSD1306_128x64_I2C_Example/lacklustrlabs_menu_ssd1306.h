/*
 * lacklustr_menu_serial.h
 *
 *  Created on: Apr 5, 2017
 *      Author: lacklustrlabs
 */
#pragma once

#include "Arduino.h"
#include "lacklustrlabs_menu.h"
#include <Adafruit_GFX.h>
#if defined(ARDUINO_ARCH_STM32F1)
#include <Adafruit_SSD1306_STM32.h>
#else
#include <Adafruit_SSD1306.h>
#endif

#define SSD1306_CHAR_HEIGHT 8

struct RenderContext {
  RenderContext() = delete;
  RenderContext( const RenderContext& other ) = delete; // non construction-copyable
  RenderContext& operator=( const RenderContext& ) = delete; // non copyable
  RenderContext(Adafruit_SSD1306& display):_display(display) {}

  Adafruit_SSD1306& _display;
  uint8_t _x=0; // origin x
  uint8_t _y=0; // origin y
  uint8_t _w=0; // width
  uint8_t _h=0; // height

  void clearRect(uint8_t x, uint8_t y) {
    _display.fillRect(x, y, _w, _h, BLACK);
  }
};

namespace lacklustrlabs {

template<>
void MenuItem<RenderContext>::renderV(RenderContext& context) const {
  // assume display.setCursor(x,y); is done in Menu::render()
  context._display.println(getNameV());
}

template<>
void NumericMenuItem<RenderContext>::renderV(RenderContext& context) const {
  // assume display.setCursor(x,y); is done in Menu::render()
  uint8_t x=context._x;
  uint8_t y=context._y;
  uint8_t _width=context._w/2.0;

  if (isActiveV()) {
    context._display.drawLine(x, y + (SSD1306_CHAR_HEIGHT / 3), x + _width, y + (SSD1306_CHAR_HEIGHT / 3), 1);
    int relXPos = x + int( (_width - 1) * (_value - _minValue) / (_maxValue - _minValue));
    int relYPos = y + (SSD1306_CHAR_HEIGHT / 3);
    context._display.drawLine(relXPos, relYPos - 2, relXPos, relYPos + 2, 1);

    context._display.setCursor(x + _width + SSD1306_CHAR_HEIGHT / 2, y);
    context._display.print(getNameV());
  } else {
    context._display.print(getNameV());
  }
}

template<>
void Menu<RenderContext>::setupContext() {
  _context._x = 0;
  _context._y = 0;
  _context._w = _context._display.width();
  _context._h = _context._display.height() / 4;
}

template<>
void Menu<RenderContext>::render() {
  SubMenu<RenderContext> *parent = _current->getParent();

  if (parent) {
    _context._y = 0;
    _context._display.setCursor(0,_context._y);
    _context.clearRect(0,_context._y);
    parent->renderV(_context);
  }

  _context._y = _context._h;
  _context._display.setCursor(0,_context._y);
  _context.clearRect(0,_context._y);
  _current->renderV(_context);
  _context._display.display();
}

}

String boolFormat(const lacklustrlabs::NumericMenuItem<RenderContext>* item) {
  if (item->isActiveV())
   return String((bool)item->getValue());
  else
   return item->getName() + " " + String((bool)item->getValue());
}

String floatFormat(const lacklustrlabs::NumericMenuItem<RenderContext>* item) {
  if (item->isActiveV())
   return String(item->getValue());
  else
   return item->getName() + " " + String(item->_value);
}

String intFormat(const lacklustrlabs::NumericMenuItem<RenderContext>* item) {
  if (item->isActiveV())
   return String((int)item->getValue());
  else
   return item->getName() + " " + String((int)item->getValue());
}
