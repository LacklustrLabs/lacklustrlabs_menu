#include "Arduino.h"
#include "lacklustrlabs_menu_serial.h"

using namespace lacklustrlabs;

RenderContext context(Serial);
typedef MenuItem<RenderContext> MI;
typedef BackMenuItem<RenderContext> BMI;
typedef ToggleMenuItem<RenderContext> TMI;
typedef NumericMenuItem<RenderContext> NMI;
typedef SubMenu<RenderContext> SM;

void callback(NMI* menuitem) {
  Serial.print("NumericMenuItem changed ptr=");
  Serial.print(reinterpret_cast<uintptr_t>(menuitem));
  Serial.print(", value= ");
  Serial.println(menuitem->_value);
}

void callback2(TMI* menuitem) {
  Serial.print("ToggleMenuItem changed ptr=");
  Serial.print(reinterpret_cast<uintptr_t>(menuitem));
  Serial.print(", name=\"");
  Serial.print(menuitem->getNameV());
  Serial.print("\", toggle=");
  Serial.println(menuitem->getToggle());
}

MI* subMenuItems2[] = {
    new BMI("<-- back"),
    new MI("sub2 first"),
    new NMI("sub2 bool", 0, 0, 1, 1, boolFormat, callback),
    new NMI("sub2 float", 5, 0, 10,-.5, floatFormat), // will decrement on 'up event'
    new NMI("sub2 int", 5, 0, 10, 1, intFormat),      // will increment on 'up event'
    new MI("sub2 last"), nullptr };

SM subMenu2("2 Sub menu 2", subMenuItems2);

MI* subMenuItems1[] = {
    new BMI("<-- back"),
    &subMenu2,
    new MI("sub1 first"),
    new NMI("sub1 bool", 0, 0, 1, 1, boolFormat),
    new NMI("sub1 float", 5, 0, 10, -.5, floatFormat), // will decrement on 'up event'
    new NMI("sub1 int", 5, 0, 10, 1, intFormat),       // will increment on 'up event'
    new MI("sub1 last"), nullptr };

SM subMenu1("1 Sub menu 1", subMenuItems1);

MI* menuItems[] = {
    &subMenu1,
    new MI("0 first"),
    new TMI("toggle 1", "toggle 2", callback2),
    new NMI("0 bool", 0, 0, 1, 1, boolFormat, callback),
    new NMI("0 float", 5, 0, 10,-.5, floatFormat), // will decrement on 'up event'
    new NMI("0 int", 5, 0, 10, 1, intFormat),      // will increment on 'up event'
    new MI("0 last"), nullptr };

Menu<RenderContext> aMenu(context, "0 Main Menu", menuItems);

void setup() {
  Serial.begin(115200);
  aMenu.render();
}

void loop() {
  char c = Serial.read();
  bool updated = false;
  switch (c) {
  case 'w':
    aMenu.upEvent();
    updated = true;
    break;
  case 's':
    aMenu.downEvent();
    updated = true;
    break;
  case 'd':
    aMenu.selectEvent();
    updated = true;
    break;
  case 'a':
    aMenu.backEvent();
    updated = true;
    break;
  case 'f':
    // will set NumericMenuItems to default value
    aMenu.longPressEvent();
    updated = true;
    break;
#ifndef ARDUINO
  case 'q':
    Serial.println(F("bye"));
    exit(1);
#endif
  }
  if (updated) {
    aMenu.render();
  }

}
